<?php
require_once "ConnexionBaseDeDonnees.php";
require_once "Utilisateur.php";
    $user = null;
    if (isset($_POST['EnvoyerPost'])){
        $user = new Utilisateur($_POST['login'], $_POST['nom'], $_POST['prenom']);
        $user->ajouter();
    } elseif (isset($_POST["EnvoyerPostLogin"])){
        $user = Utilisateur::recupererUtilisateurParLogin($_POST['login']);
    }
    else {
        $user = Utilisateur::recupererUtilisateurParLogin("login1");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire Voiture</title>
</head>
<body>
<!--<pre>
        <?=var_dump($_POST)?>
    </pre>-->
    <h3>Avec post</h3>
    <?php if (isset($_POST["EnvoyerPost"])):?>
        <?php if($user != NULL):?>
            <h3>L'utilisateur n'existe pas</h3>
        <?php else:?>
            <?=$user?>
        <?php endif;?>
    <?php else: ?>
        <form method="post" action="./testRequetePrepare.php">
            <fieldset>
                <legend>Formulaire creer un Utilisateur:</legend>
                <p>
                    <label for="login">Login :</label>
                    <input type="text" placeholder="leconbee" name="login" id="login" required/>
                </p>
                <p>
                    <label for="nom">Nom :</label>
                    <input type="text" placeholder="nom" name="nom" id="nom" required/>
                </p>
                <p>
                    <label for="prenom">Prenom :</label>
                    <input type="text" placeholder="prenom" name="prenom" id="prenom" required/>
                </p>
                <p>
                    <input type="submit" name="EnvoyerPost" value="EnvoyerPost" />
                </p>
            </fieldset>
        </form>
        <form method="post" action="./testRequetePrepare.php">
            <fieldset>
                <legend>Trouver Utilisateur par login:</legend>
                <p>
                    <label for="login">Login :</label>
                    <input type="text" placeholder="leconbee" name="login" id="login" required/>
                </p>
                <p>
                    <input type="submit" name="EnvoyerPostLogin" value="EnvoyerPostLogin" />
                </p>
            </fieldset>
        </form>
    <?php endif; ?>
</body>
</html>
