<?php
/** @var string $preference */
?>

<form method="POST" action="./controleurFrontal.php?action=enregistrerPreference">
    <fieldset>
        <legend>Preferences de formulaire</legend>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
        <?php if($preference == 'utilisateur') echo 'checked';?>
        >
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
            <?php if($preference == 'trajet') echo 'checked';?>>
        <label for="trajetId">Trajet</label>
    </fieldset>
    <input type="submit" name="Enregistrer">
</form>
