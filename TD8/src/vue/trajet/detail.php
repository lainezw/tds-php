<?php

/** @var App\Covoiturage\Modele\DataObject\Trajet $trajet */

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste des trajets</title>
</head>
<body>
    <h1>Details sur le trajet <?=htmlspecialchars($trajet->getId())?></h1>
    <p><?=htmlspecialchars($trajet)?></p>
</body>
</html>
