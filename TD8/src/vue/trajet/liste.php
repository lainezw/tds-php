<?php
/** @var App\Covoiturage\Modele\DataObject\Trajet[] $trajets */
?>

<h1>Liste d'Trajets</h1>
<?php if (ConnexionUtilisateur::estConnecte()): ?>
    <?php foreach ($trajets as $trajet):?>
        <p>Trajet
            <a href="./controleurFrontal.php?controleur=Trajet&action=afficherDetail&id=<?=rawurlencode($trajet->getId())?>">
                <?=htmlspecialchars($trajet->getId())?>
            </a>
            <a href="./controleurFrontal.php?controleur=Trajet&action=supprimer&id=<?=rawurlencode($trajet->getId())?>">(-delete)</a>
                 <a href="./controleurFrontal.php?controleur=Trajet&action=formulaireMiseAJour&id=<?=rawurlencode($trajet->getId())?>">(Modifier)</a>
        </p>
    <?php endforeach;?>
<?php else: ?>
    <?php foreach ($trajets as $trajet):?>
        <p>Trajet
            <a href="./controleurFrontal.php?controleur=Trajet&action=afficherDetail&id=<?=rawurlencode($trajet->getId())?>">
                <?=htmlspecialchars($trajet->getId())?>
            </a>
        </p>
    <?php endforeach;?>
<?php endif;?>
