<?php
/** @var App\Covoiturage\Modele\Utilisateur[] $utilisateurs */

use App\Covoiturage\Lib\ConnexionUtilisateur;

?>

<h1>Liste d'Utilisateurs</h1>
<?php if (ConnexionUtilisateur::estConnecte()): ?>
    <?php foreach ($utilisateurs as $utilisateur):?>
        <p>Utilisateur
            <a
                    href="./controleurFrontal.php?controleur=Utilisateur&action=afficherDetail&login=<?=rawurlencode($utilisateur->getLogin())?>">
                <?=htmlspecialchars($utilisateur->getNom())?>
            </a>
            <?php if (ConnexionUtilisateur::getLoginUtilisateurConnecte() == $utilisateur->getLogin() || ConnexionUtilisateur::estAdministrateur()): ?>
                <a
                        href="./controleurFrontal.php?controleur=Utilisateur&action=supprimer&login=<?=rawurlencode($utilisateur->getLogin())?>">(-delete)
                </a>
                <a
                        href="./controleurFrontal.php?controleur=Utilisateur&action=formulaireMiseAJour&login=<?=rawurlencode($utilisateur->getLogin())?>">(Modifier)
                </a>
            <?php endif;?>
        </p>
    <?php endforeach;?>
<?php else: ?>
    <?php foreach ($utilisateurs as $utilisateur):?>
        <p>Utilisateur
            <a href="./controleurFrontal.php?controleur=Utilisateur&action=afficherDetail&login=<?=rawurlencode($utilisateur->getLogin())?>">
                <?=htmlspecialchars($utilisateur->getNom())?>
            </a>
        </p>
    <?php endforeach;?>
<?php endif; ?>
