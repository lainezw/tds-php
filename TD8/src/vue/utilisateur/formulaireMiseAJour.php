<?php

/** @var App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */

use App\Covoiturage\Lib\ConnexionUtilisateur;

?>

<h1>Bienvenue sur le formulaire de mise à jour d'utilisateur</h1>

<!-- Formulaire -->
<?php if (ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::getLoginUtilisateurConnecte() == $utilisateur->getLogin()) : ?>
    <form method="get" action="./controleurFrontal.php">
        <fieldset>
            <legend>Mon formulaire :</legend>
            <p class="InputAddOn">
                <input type="hidden" name="action" value="afficherFormulaireMiseAJour"/>
                <input type="hidden" name="controleur" value="Utilisateur"/>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="nom_id">Nom</label> :
                <input type="text" placeholder="Leblanc" name="nom" id="nom_id" value="<?=$utilisateur->getNom()?>" required />
            </p>

            <p class="InputAddOn">
                <label class="InputAddOn-item" for="prenom_id">Prénom</label> :
                <input type="text" placeholder="Juste" name="prenom" id="prenom_id" value="<?=$utilisateur->getPrenom()?>" required />
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" <?php if ($utilisateur->isEstAdmin()) echo "checked"?>>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="login_id">Login</label> :
                <input type="text" placeholder="leblancj" name="login" id="login_id" value="<?=$utilisateur->getLogin()?>" required />
            </p>
            <p class="InputAddOn">
                <input type="submit" value="Envoyer" />
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="mdp_id">Ancien mot de passe&#42;</label>
                <input class="InputAddOn-field" type="password" value="" placeholder="" name="Amdp" id="mdp_id" required>
            </p>

            <p class="InputAddOn">
                <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe&#42;</label>
                <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
                <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
            </p>
        </fieldset>
    </form>
<?php endif; ?>