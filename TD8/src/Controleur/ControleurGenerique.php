<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        $login = ConnexionUtilisateur::estConnecte();
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    protected static function afficherVueGeneralAvec(string $cheminCorpsVue, array $parametres = []): void
    {
        $login = ConnexionUtilisateur::estConnecte();
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/vueGeneral.php"; // Charge la vue
    }

    protected static function afficherErreur(string $message) :void
    {
        self::afficherVue("erreur.php", ["erreur" => $message]);
    }



    public static function afficherFormulairePreference(): void
    {
        $preference = PreferenceControleur::lire();
        self::afficherVueGeneralAvec("formulairePreference.php", ["preference" => $preference]);

    }

    public static function enregistrerPreference(): void{
        if(isset($_POST['Enregistrer'])){
            $controleur_defaut = $_POST['controleur_defaut'];
            PreferenceControleur::enregistrer($controleur_defaut);
            self::afficherVueGeneralAvec("preferenceEnregistree.php");
        }
    }
}