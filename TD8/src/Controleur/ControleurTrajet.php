<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
class ControleurTrajet
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue("vueGeneral.php", ["trajets" => $trajets, "titre" => "Liste Trajet", "cheminCorpsVue" => "trajet/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(array $get): void
    {
        $id = $get["id"];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        if (empty($trajet))
            ControleurTrajet::afficherVue("erreur.php");
        else
            ControleurTrajet::afficherVue("vueGeneral.php", ["trajet" => $trajet, "titre" => "afficherDetail", "cheminCorpsVue" => "trajet/detail.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire(array $get): void
    {
        $trajet = (new TrajetRepository())->construireDepuisTableauSQL($get);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue("vueGeneral.php", ["trajet" => $trajet, "trajets" => $trajets, "titre" => "afficherDetail", "cheminCorpsVue" => "trajet/afficherFormulaireCreation.php"]);
    }

    public static function afficherFormulaireCreaction(): void
    {
        ControleurTrajet::afficherVue("vueGeneral.php", ["titre" => "afficherDetail", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    /** $user c'est le tableau $_GET*/
    public static function formulaireMiseAJour(Array $get): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($get["id"]);
        ControleurTrajet::afficherVue("vueGeneral.php", ["trajet" => $trajet, "titre" => "Formulaire MiseAJour", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
    }

    /** $user c'est le tableau $_GET*/
    public static function afficherFormulaireMiseAJour(Array $get): void{
        $trajet = (new TrajetRepository())->construireDepuisTableauSQL($get);
        (new TrajetRepository())->miseAJours($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue("vueGeneral.php", ["trajet" => $trajet, "trajets" => $trajets, "titre" => "afficherDetail", "cheminCorpsVue" => "trajet/afficherFormulaireMiseAJour.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function trajetCree(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue("vueGeneral.php", ["trajets" => $trajets, "titre" => "Liste Trajet", "cheminCorpsVue" => "trajet/trajetCree.php"]);  //"redirige" vers la vue
    }

    public static function supprimer($get): void{
        $bienFait = false;
        if (isset($get["id"])){
            $id = $get["id"];
            $bienFait = (new TrajetRepository())->supprimer($id);
        }
        $trajets = (new TrajetRepository())->recuperer();
        ControleurTrajet::afficherVue("vueGeneral.php", ["bienFait" => $bienFait, "trajets" => $trajets, "titre" => "Liste Trajet", "cheminCorpsVue" => "trajet/trajetSupprime.php", "login" => $id]);  //"redirige" vers la vue
    }
}

?>