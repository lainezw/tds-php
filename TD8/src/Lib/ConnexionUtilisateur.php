<?php
namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        $_SESSION[self::$cleConnexion] = $loginUtilisateur; // Enregistre le login en session
    }

    public static function estConnecte(): bool
    {
        return isset($_SESSION[self::$cleConnexion]); // Vérifie si la session contient la clé de connexion
    }

    public static function deconnecter(): void
    {
        unset($_SESSION[self::$cleConnexion]); // Supprime l'enregistrement de la session
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return self::estConnecte() ? $_SESSION[self::$cleConnexion] : null; // Retourne le login ou null
    }

    public static function estAdministrateur(): bool
    {
        /** @var Utilisateur $utilisateur*/
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire(self::getLoginUtilisateurConnecte());
        return $utilisateur->isEstAdmin();
    }
}
