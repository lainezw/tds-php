<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
#use App\Covoiturage\Modele\DataObject\Trajet;

class UtilisateurRepository extends AbstractRepository
{
    public function construireDepuisTableauSQL(array $objetFormatTableau): Utilisateur
    {
        return new Utilisateur($objetFormatTableau['login'], $objetFormatTableau['nom'], $objetFormatTableau['prenom'], $objetFormatTableau['mdpHache'], isset($objetFormatTableau['estAdmin'])&&$objetFormatTableau['estAdmin'] == "on", $objetFormatTableau['email'], $objetFormatTableau['emailAValider'], $objetFormatTableau['nonce']);
    }
    protected function getNomTable(): string
    {
        return "utilisateur";
    }
    protected function getNomClePrimaire(): string
    {
        return "login";
    }
    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }
    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        /** @var Utilisateur $objet */
        return array(
            "loginTag" => $objet->getLogin(),
            "nomTag" => $objet->getNom(),
            "prenomTag" => $objet->getPrenom(),
            "mdpHacheTag" => $objet->getMdpHache(),
            "estAdminTag" => $objet->isEstAdmin(),
            "emailTag" => $objet->getEmail(),
            "emailAValiderTag" => $objet->getEmailAValider(),
            "nonceTag" => $objet->getNonce()
        );
    }
}