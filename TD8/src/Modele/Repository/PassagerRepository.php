<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Passager;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class PassagerRepository extends AbstractRepository
{
    public static function recupererPassagers(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM passager");
        $passagers = [];
        foreach ($pdoStatement as $passager) {
            $passagers[] = PassagerRepository::construireDepuisTableauSQL($passager);
        }
        return $passagers;
    }

    public static function recupererPassagersParIdTraje(int $trajeId): array
    {
        $sql = "SELECT * FROM passager WHERE trajetid = :trajeId";
        $values = array(
            "trajeId" => $trajeId,
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute($values);
        $passagers = [];
        foreach ($pdoStatement as $passager) {
            $passagers[] = PassagerRepository::construireDepuisTableauSQL($passager)->getUtilisateur();
        }
        return $passagers;
    }

    public static function recupererPassagersParLogin(string $login): array
    {
        $sql = "SELECT * FROM passager WHERE passagerLogin = :login";
        $values = array(
            "login" => $login,
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute($values);
        $passagers = [];
        foreach ($pdoStatement as $passager) {
            $passagers[] = (new PassagerRepository())->construireDepuisTableauSQL($passager)->getTrajet();
        }
        return $passagers;
    }

    public function construireDepuisTableauSQL(array $objetFormatTableau): Passager
    {
        return new Passager(
            TrajetRepository::recupererTrajetParId($objetFormatTableau['trajetId']),
            UtilisateurRepository::recupererParClePrimaire($objetFormatTableau['passagerLogin'])
        );
    }

    public function ajouter(Passager $passager): void
    {
        $sql = "INSERT INTO passager (trajetId, passagerLogin) VALUES (:trajeId, :passagerLogin)";
        $values = array(
            "trajeId" => $passager->getTrajet()->getId(),
            "passagerLogin" => $passager->getUtilisateur()->getLogin()
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute($values);
    }

    protected function getNomTable(): string
    {
        // TODO: Implement getNomTable() method.
    }
}