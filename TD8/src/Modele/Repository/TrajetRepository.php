<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateMalformedStringException;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    /**
     * @throws DateMalformedStringException
     */
    public function construireDepuisTableauSQL(array $objetFormatTableau): Trajet
    {
        /** @noinspection PhpParamsInspection */
        return new Trajet(
            isset($objetFormatTableau["id"]) ? (int) $objetFormatTableau["id"] : null,
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            (new DateTime($objetFormatTableau["date"])), // À changer
            $objetFormatTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($objetFormatTableau["conducteurLogin"]), // À changer
            $objetFormatTableau["nonFumeur"] == "on" ? 1 : $objetFormatTableau["nonFumeur"], // À changer ?
            $objetFormatTableau["nbPlaces"] // À changer
        );
    }
    protected function getNomTable(): string
    {
        return "trajet";
    }
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "nbPlaces", "prix", "conducteurLogin", "nonFumeur"];
    }
    protected function formatTableauSQL(AbstractDataObject $objet): array
    {
        return array(
            /** @var Trajet $objet */
            "idTag" => $objet->getId(),
            "departTag" => $objet->getDepart(),
            "arriveeTag" => $objet->getArrivee(),
            "dateTag" => $objet->getDate()->format('Y-m-d'),
            "nbPlacesTag" => $objet->getNbPlaces(),
            "prixTag" => $objet->getPrix(),
            "conducteurLoginTag" => $objet->getConducteur()->getLogin(),
            "nonFumeurTag" => $objet->isNonFumeur() ? 0 : 1,
        );
    }
    protected function getNomClePrimaire(): string
    {
        return "id";
    }
    private static function recupererPassagers(Trajet $trajet): array
    {
        if ($trajet->getPassagers() == null) {
            $trajet->setPassagers(PassagerRepository::recupererPassagersParIdTraje($trajet->getId()));
        }
        return $trajet->getPassagers();
    }
    public function supprimerPassager(string $passagerLogin, Trajet $trajet): bool
    {
        $sql = "DELETE FROM passager WHERE trajetId = :trajetId AND passagerLogin = :passagerLogin";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(array(
            "trajetId" => $trajet->getId(),
            "passagerLogin" => $passagerLogin,
        ));
        return $pdoStatement->rowCount() > 0;
    }
}