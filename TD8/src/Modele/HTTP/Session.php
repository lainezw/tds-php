<?php
namespace App\Covoiturage\Modele\HTTP;

use Exception;

class Session
{
    private static ?Session $instance = null;
    private const DUREE_EXPIRATION = 1800; // Durée d'expiration en secondes (30 minutes)

    /**
     * @throws Exception
     */
    private function __construct()
    {
        if (session_start() === false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }
    }

    public static function getInstance(): Session
    {
        if (is_null(Session::$instance)) {
            Session::$instance = new Session();
            Session::$instance->verifierDerniereActivite(); // Vérifier l'expiration
        }
        return Session::$instance;
    }

    private function verifierDerniereActivite(): void
    {
        if (isset($_SESSION['derniere_activite'])) {
            $inactivite = time() - $_SESSION['derniere_activite'];
            if ($inactivite > self::DUREE_EXPIRATION) {
                $this->detruire(); // Détruire la session si elle est expirée
            }
        }
        $_SESSION['derniere_activite'] = time(); // Mettre à jour la dernière activité
    }

    public function contient($nom): bool
    {
        return isset($_SESSION[$nom]);
    }

    public function enregistrer(string $nom, mixed $valeur): void
    {
        $_SESSION[$nom] = $valeur;
    }

    public function lire(string $nom): mixed
    {
        return $_SESSION[$nom] ?? null;
    }

    public function supprimer($nom): void
    {
        unset($_SESSION[$nom]);
    }

    public function detruire(): void
    {
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        Cookie::supprimer(session_name()); // deletes the session cookie
        Session::$instance = null; // Réinitialiser l'instance
    }
}
