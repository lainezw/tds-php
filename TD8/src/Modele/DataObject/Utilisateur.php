<?php
namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject
{

    private string $prenom;
    private string $nom;
    private string $login;
    private string $mdpHache;
    private bool $estAdmin;
    private ?string $email;
    private ?string $emailAValider;
    private ?string $nonce;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    public function getTrajetsCommePassager(): ?array
    {
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = $this->verificationLongeur($login);
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom,
        $mdpHache,
        $estAdmin,
        ?string $email,
        ?string $emailAValider,
        ?string $nonce,
    )
    {
        $this->login = $this->verificationLongeur($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
        $this->trajetsCommePassager = null;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string
    {
        return 'Utilisateur '.$this->prenom.', '.$this->nom.' de login '.$this->login;
    }

    public function verificationLongeur(string $login): string
    {
        if (strlen($login) > 64)
            return substr($login, 0, (64 - strlen($login)));
        else
            return $login;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAValider(): ?string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(?string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): ?string
    {
        return $this->nonce;
    }

    public function setNonce(?string $nonce): void
    {
        $this->nonce = $nonce;
    }
}