<?php


require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';


$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src/');

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\HTTP\Session;

$session = Session::getInstance();
$login = ConnexionUtilisateur::estConnecte();

if (isset($_GET['action'])){
    $controleur = null;
    $action = $_GET['action'];
    if (isset($_GET['controleur'])){
        $controleur = ucfirst($_GET['controleur']);
        $controleur = 'App\Covoiturage\Controleur\Controleur'.ucfirst($_GET['controleur']);
    }
    elseif (isset($_COOKIE['preferenceControleur']))
        $controleur = 'App\Covoiturage\Controleur\Controleur'.ucfirst(unserialize($_COOKIE['preferenceControleur']));

    if (class_exists($controleur) && method_exists($controleur, $action))
        call_user_func([$controleur, $action], $_GET);
    elseif (method_exists('App\Covoiturage\Controleur\ControleurGenerique', $action))
        call_user_func(['App\Covoiturage\Controleur\ControleurGenerique', $action], $_GET);
    else
        echo "<br>Clase o método no existe.";
}
else
    echo "<br>Clase o método no existe.";
?>