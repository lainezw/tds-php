<h1>Bienvenue sur le formulaire de création de Trajet</h1>

<!-- Formulaire -->
<form method="get" action="./controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <input type="hidden" name="action" value="creerDepuisFormulaire">
            <input type="hidden" name="controleur" value="trajet">
        </p>
        <p class="InputAddOn">
            <label for="depart">Depart</label> :
            <input type="text" placeholder="Montpellier" name="depart" id="depart" required/>
        </p>
        <p class="InputAddOn">
            <label for="arrivee">Arrivée</label> :
            <input type="text" placeholder="Sète" name="arrivee" id="arrivee" required/>
        </p>
        <p class="InputAddOn">
            <label for="date">Date</label> :
            <input type="date" placeholder="JJ/MM/AAAA" name="date" id="date"  required/>
        </p>
        <p class="InputAddOn">
            <label for="prix">Prix</label> :
            <input type="number" placeholder="200" name="prix" id="prix"  required/>
        </p>
        <p class="InputAddOn">
            <label for="nbPlaces">Nombre de places</label> :
            <input type="number" placeholder="18" name="nbPlaces" id="nbPlaces"  required/>
        </p>
        <p class="InputAddOn">
            <label for="conducteurLogin">Login du conducteur</label> :
            <input type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin" required/>
        </p>
        <p class="InputAddOn">
            <label for="nonFumeur">Non Fumeur ?</label> :
            <input type="checkbox"  name="nonFumeur" id="nonFumeur"/>
        </p>
        <p class="InputAddOn">
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>