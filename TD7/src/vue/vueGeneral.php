<?php
/** @var string $titre*/
/** @var string $cheminCorpsVue*/

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <title><?php echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="./controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="./controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="./controleurFrontal.php?action=afficherFormulairePreference">
                    <img src="https://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/TD7/heart.png">
                </a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de...
    </p>
</footer>
</body>
</html>