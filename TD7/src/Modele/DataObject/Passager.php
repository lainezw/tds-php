<?php

namespace App\Covoiturage\Modele\DataObject;

class Passager extends AbstractDataObject
{
    private Trajet $trajet;
    private Utilisateur $utilisateur;

    public function __construct(Trajet $trajet, Utilisateur $utilisateur)
    {
        $this->trajet = $trajet;
        $this->utilisateur = $utilisateur;
    }

    public function getTrajet()
    {
        return $this->trajet;
    }

    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    public function setTrajet(Trajet $trajet)
    {
        $this->trajet = $trajet;
    }

    public function setUtilisateur(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    public function __toString()
    {
        return "$this->trajet ($this->utilisateur)";
    }
}