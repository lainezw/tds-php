<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void{

        //$valeur = json_encode($valeur);
        $valeur = serialize($valeur);
        if(is_null($dureeExpiration)){
            $dureeExpiration = 0;
        } else
            $dureeExpiration = time() + $dureeExpiration;
        setcookie($cle, $valeur, $dureeExpiration);
    }

    public static function lire(string $cle): mixed {
        //return json_decode($_COOKIE[$cle]);
        return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool{
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void{
        unset($_COOKIE[$cle]);
    }
}