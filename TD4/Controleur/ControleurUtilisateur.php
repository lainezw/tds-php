<?php

#namespace Controleur;
use src\Modele\ModeleUtilisateur;

require_once ("./../Modele/ModeleUtilisateur.php"); // chargement du modèle
class ControleurUtilisateur
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        \src\Controleur\ControleurUtilisateur::afficherVue("liste.php", ["utilisateurs"=>$utilisateurs,]);  //"redirige" vers la vue
    }

    public static function afficherDetail(String $login) : void {
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
        if (empty($utilisateur))
            \src\Controleur\ControleurUtilisateur::afficherVue("erreur.php");
        else
            \src\Controleur\ControleurUtilisateur::afficherVue("detail.php", ["utilisateur" => $utilisateur,]);  //"redirige" vers la vue
    }

    public static function afficherFormulaireCreation(array $user) : void
    {
        $utilisateur = ModeleUtilisateur::construireDepuisTableauSQL($user);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        \src\Controleur\ControleurUtilisateur::afficherVue("afficherFormulaireCreation.php", ["utilisateur" => $utilisateur,"utilisateurs"=>$utilisateurs,]);
    }

    public static function afficherFormulaire() : void{
        \src\Controleur\ControleurUtilisateur::afficherVue("formulaireCreation.php");
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/utilisateur/$cheminVue"; // Charge la vue
    }
}
?>