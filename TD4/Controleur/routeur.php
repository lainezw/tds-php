<?php

use src\Controleur\ControleurUtilisateur;

require_once './ControleurUtilisateur.php';
$action = $_GET['action'];
//echo $action;
switch ($action) {
    case 'afficherListe':
        ControleurUtilisateur::$action(); // Appel de la méthode statique $action de ControleurUtilisateur\\
        break;
    case 'afficherDetail':
        ControleurUtilisateur::afficherDetail($_GET['login']);
        break;
    case 'creerDepuisFormulaire':
        ControleurUtilisateur::afficherFormulaire();
        break;
    case 'afficherFormulaireCreation':
        ControleurUtilisateur::afficherFormulaireCreation($_GET);
        break;
    default:
        echo "C quoi ton probleme??";
        break;
}

?>