<?php

#namespace Modele;
use src\Modele\ConnexionBaseDeDonnees;

require_once "ConnexionBaseDeDonnees.php";
require_once "ModeleTrajet.php";
require_once "ModeleUtilisateur.php";

class ModelePassager
{
    private Trajet $trajet;
    private Utilisateur $utilisateur;

    public function __construct(Trajet $trajet, Utilisateur $utilisateur)
    {
        $this->trajet = $trajet;
        $this->utilisateur = $utilisateur;
    }

    public function getTrajet()
    {
        return $this->trajet;
    }

    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    public function setTrajet(Trajet $trajet)
    {
        $this->trajet = $trajet;
    }

    public function setUtilisateur(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    public function __toString()
    {
        return "$this->trajet ($this->utilisateur)";
    }

    public static function recupererPassagers(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM passager");
        $passagers = [];
        foreach ($pdoStatement as $passager) {
            $passagers[] = \src\Modele\ModelePassager::construireDepuisTableauSQL($passager);
        }
        return $passagers;
    }

    public static function recupererPassagersParIdTraje($trajeId): array
    {
        $sql = "SELECT * FROM passager WHERE trajetid = :trajeId";
        $values = array(
            "trajeId" => $trajeId,
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute($values);
        $passagers = [];
        foreach ($pdoStatement as $passager) {
            $passagers[] = \src\Modele\ModelePassager::construireDepuisTableauSQL($passager)->getUtilisateur();
        }
        return $passagers;
    }

    public static function recupererPassagersParLogin($login): array
    {
        $sql = "SELECT * FROM passager WHERE passagerLogin = :login";
        $values = array(
            "login" => $login,
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute($values);
        $passagers = [];
        foreach ($pdoStatement as $passager) {
            $passagers[] = \src\Modele\ModelePassager::construireDepuisTableauSQL($passager)->getTrajet();
        }
        return $passagers;
    }

    public static function construireDepuisTableauSQL(array $passagerTableau): \src\Modele\ModelePassager
    {
        return new \src\Modele\ModelePassager(
            Trajet::recupererTrajetParId($passagerTableau['trajetId']),
            Utilisateur::recupererUtilisateurParLogin($passagerTableau['passagerLogin'])
        );
    }

    public function ajouter(): void
    {
        $sql = "INSERT INTO passager (trajetId, passagerLogin) VALUES (:trajeId, :passagerLogin)";
        $values = array(
            "trajeId" => $this->trajet->getId(),
            "passagerLogin" => $this->utilisateur->getLogin()
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare($sql);
        $pdoStatement->execute($values);
    }
}