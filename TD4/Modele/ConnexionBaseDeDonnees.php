<?php

use src\Configuration\ConfigurationBaseDeDonnees;

require_once "/var/www/html/tds-php/TD4/Configuration/ConfigurationBaseDeDonnees.php";

class ConnexionBaseDeDonnees
{
    private static ?\src\Modele\ConnexionBaseDeDonnees $instance = null;
    private PDO $pdo;

    public function __construct()
    {
        //$ConnexionBaseDeDonnees = new ConfigurationBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $motDePasse = ConfigurationBaseDeDonnees::getMotDePasse();
        $this->pdo = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /*public function setPDO(PDO $pdo) {
        $this->pdo = $pdo;
    }*/
    public static function getPDO(): PDO
    {
        return \src\Modele\ConnexionBaseDeDonnees::getInstance()->pdo;
    }

    private static function getInstance(): \src\Modele\ConnexionBaseDeDonnees
    {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(\src\Modele\ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur
            \src\Modele\ConnexionBaseDeDonnees::$instance = new \src\Modele\ConnexionBaseDeDonnees();
        return \src\Modele\ConnexionBaseDeDonnees::$instance;
    }
}
