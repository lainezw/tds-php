<?php

namespace Modele;

use src\Modele\ConnexionBaseDeDonnees;

require_once "ModeleUtilisateur.php";
require_once "ModelePassager.php";
require_once "ConnexionBaseDeDonnees.php";

class ModeleUtilisateur
{

    private string $prenom;
    private string $nom;
    private string $login;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    public function getTrajetsCommePassager(): ?array
    {
        return $this->recupererTrajetsCommePassager();
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = $this->verificationLongeur($login);
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom,
    )
    {
        $this->login = $this->verificationLongeur($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string
    {
        return "Utilisateur $this->prenom, $this->nom de login $this->login";
    }

    public function verificationLongeur(string $login): string
    {
        if (strlen($login) > 64)
            return substr($login, 0, (64 - strlen($login)));
        else
            return $login;
    }


    public static function recupererUtilisateurs(): array|null
    {
        $tableau = array();
        $model = new ConnexionBaseDeDonnees();
        $pdo = $model->getPDO();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = \src\Modele\ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableau;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): \src\Modele\ModeleUtilisateur
    {
        return new \src\Modele\ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }

    public static function recupererUtilisateurParLogin(string $login): ?\src\Modele\ModeleUtilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login='$login'";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        $tableau = $pdoStatement->fetch();
        if (isset($tableau['login']))
            return \src\Modele\ModeleUtilisateur::construireDepuisTableauSQL($tableau);
        else
            return null;
    }

    public function ajouter(): void
    {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
        );
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute($values);
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager(): array
    {
        if ($this->trajetsCommePassager == null) {
            $this->trajetsCommePassager = Passager::recupererPassagersParLogin($this->login);
        }
        return $this->trajetsCommePassager;
    }
}