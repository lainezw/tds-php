<?php

/** @var ModeleUtilisateur $utilisateur */

use src\Modele\ModeleUtilisateur;

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
    <h1>Details sur l'utilisateur M.<?=$utilisateur->getNom() ?></h1>
    <p><?=$utilisateur?></p>
</body>
</html>
