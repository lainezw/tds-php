<?php
#require_once "./../../Modele/Utilisateur.php";

#$utilisateurs = Utilisateur::recupererUtilisateurs();

/** @var ModeleUtilisateur[] $utilisateurs */

use src\Modele\ModeleUtilisateur;

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<h1>Liste d'Utilisateurs</h1>
<?php foreach ($utilisateurs as $utilisateur):?>
    <p>
        Utilisateur
        <a
                href="http://localhost:8666/tds-php/TD4/Controleur/routeur.php?action=afficherDetail&login=<?=$utilisateur->getLogin()?>">
            <?=$utilisateur->getNom()?>
        </a>
    </p>
<?php endforeach;?>
</body>
</html>