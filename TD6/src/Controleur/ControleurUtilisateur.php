<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
class ControleurUtilisateur
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur

    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(array $user): void
    {
        $login = $user["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (empty($utilisateur))
            ControleurUtilisateur::afficherVue("erreur.php");
        else
            ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire(array $user): void
    {
        $utilisateur = (new UtilisateurRepository())->construireDepuisTableauSQL($user);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/afficherFormulaireCreation.php"]);
    }

    public static function afficherFormulaireCreaction(): void
    {
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    /** $user c'est le tableau $_GET*/
    public static function formulaireMiseAJour(Array $get): void
    {

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($get["login"]);
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "titre" => "Formulaire MiseAJour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }

    public static function afficherFormulaireMiseAJour(Array $get): void{
        $utilisateur = (new UtilisateurRepository())->construireDepuisTableauSQL($get);
        (new UtilisateurRepository())->miseAJours($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/afficherFormulaireMiseAJours.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function utilisateurCree(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);  //"redirige" vers la vue
    }

    public static function supprimer($get): void{
        $userSupprime = false;
        if (isset($get["login"])){
            $login = $get["login"];
            $userSupprime = (new UtilisateurRepository())->supprimer($login);
        }
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["bienFait" => $userSupprime, "utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "login" => $login]);  //"redirige" vers la vue
    }
}

?>