<?php

/** @var App\Covoiturage\Modele\DataObject\Trajet $trajet */
use App\Covoiturage\Modele\DataObject\Utilisateur;
?>

<h1>Bienvenue sur le formulaire de mise à jour de trajet</h1>

<!-- Formulaire -->
<form method="get" action="./controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <input type="hidden" name="action" value="afficherFormulaireMiseAJour">
            <input type="hidden" name="controleur" value="trajet">
            <input type="hidden" name="id" value="value="<?=$trajet->getId()?>"">
        </p>
        <p class="InputAddOn">
            <label for="depart">Depart</label> :
            <input type="text" placeholder="Montpellier" name="depart" id="depart" value="<?=$trajet->getDepart()?>" required/>
        </p>
        <p class="InputAddOn">
            <label for="arrivee">Arrivée</label> :
            <input type="text" placeholder="Sète" name="arrivee" id="arrivee" value="<?=$trajet->getArrivee()?>" required/>
        </p>
        <p class="InputAddOn">
            <label for="date">Date</label> :
            <input type="date" placeholder="JJ/MM/AAAA" name="date" id="date" value="<?=$trajet->getDate()->format('Y-m-d')?>" required/>
        </p>
        <p class="InputAddOn">
            <label for="prix">Prix</label> :
            <input type="number" placeholder="200" name="prix" id="prix" value="<?=$trajet->getPrix()?>" required/>
        </p>
        <p class="InputAddOn">
            <label for="nbPlaces">Nombre de places</label> :
            <input type="number" placeholder="18" name="nbPlaces" id="nbPlaces" value="<?=$trajet->getNbPlaces()?>" required/>
        </p>
        <p class="InputAddOn">
            <label for="conducteurLogin">Login du conducteur</label> :
            <input type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin" value="<?=$trajet->getConducteur()->getLogin()?>" required/>
        </p>
        <p class="InputAddOn">
            <label for="nonFumeur">Non Fumeur ?</label> :
            <input type="checkbox"  name="nonFumeur" id="nonFumeur" <?=$trajet->isNonFumeur() ? "checked" : ""?>/>
        </p>
        <p class="InputAddOn">
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
