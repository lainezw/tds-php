<?php
/** @var App\Covoiturage\Modele\Utilisateur[] $utilisateurs */
?>

<h1>Liste d'Utilisateurs</h1>
<?php foreach ($utilisateurs as $utilisateur):?>
    <p>Utilisateur
        <a href="./controleurFrontal.php?controleur=Utilisateur&action=afficherDetail&login=<?=rawurlencode($utilisateur->getLogin())?>">
            <?=htmlspecialchars($utilisateur->getNom())?>
        </a>
        <a href="./controleurFrontal.php?controleur=Utilisateur&action=supprimer&login=<?=rawurlencode($utilisateur->getLogin())?>">(-delete)</a>
             <a href="./controleurFrontal.php?controleur=Utilisateur&action=formulaireMiseAJour&login=<?=rawurlencode($utilisateur->getLogin())?>">(Modifier)</a>
    </p>
<?php endforeach;?>