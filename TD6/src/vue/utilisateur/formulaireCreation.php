<h1>Bienvenue sur le formulaire de création d'utilisateur</h1>

<!-- Formulaire -->
<form method="get" action="./controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <input type="hidden" name="action" value="creerDepuisFormulaire">
            <input type="hidden" name="controleur" value="Utilisateur">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <input type="text" placeholder="Leblanc" name="nom" id="nom_id" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom</label> :
            <input type="text" placeholder="Juste" name="prenom" id="prenom_id" required />
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input type="text" placeholder="leblancj" name="login" id="login_id" required />
        </p>
        <p class="InputAddOn">
            <input type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>