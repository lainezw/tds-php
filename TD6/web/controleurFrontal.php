<?php
require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';


$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src/');

if (isset($_GET['action']) && isset($_GET['controleur'])){
    $action = $_GET['action'];
    $controleur = ucfirst($_GET['controleur']);
    $controleur = 'App\Covoiturage\Controleur\Controleur'.ucfirst($_GET['controleur']);
    if (class_exists($controleur) && method_exists($controleur, $action)) {
        $controleur = 'App\\Covoiturage\\Controleur\\Controleur' . ucfirst($_GET['controleur']);
        call_user_func([$controleur, $action], $_GET);
    }
} else
    echo "<br>Clase o método no existe.";
?>