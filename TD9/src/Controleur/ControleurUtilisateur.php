<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
class ControleurUtilisateur extends ControleurGenerique
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur

    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        parent::afficherVue("vueGeneral.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(array $user): void
    {
        $login = $user["login"] ?? "";
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (empty($utilisateur)){
            MessageFlash::ajouter('warning','Login inconnu');
            header("location: /tds-php/TD9/web/controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        }
        else
            parent::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire(array $user): void
    {

        if ($user['mdp'] != $user['mdp2'])
            parent::afficherErreur("Mots de passe distincts.");
        else{
            $user['emailAValider'] = $user['email'];
            $user['email'] = "";
            $user['nonce'] = MotDePasse::genererChaineAleatoire();
            $mdpHache = MotDePasse::hacher($user['mdp']);
            $user['mdpHache'] = $mdpHache;
            $utilisateur = (new UtilisateurRepository())->construireDepuisTableauSQL($user);
            (new UtilisateurRepository())->ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            parent::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/afficherFormulaireCreation.php"]);
        }

    }

    public static function validerEmail(array $get): void{

    }

    public static function afficherFormulaireCreaction(): void
    {
        parent::afficherVue("vueGeneral.php", ["titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    /** $user c'est le tableau $_GET*/
    public static function formulaireMiseAJour(Array $get): void
    {
        if (!ConnexionUtilisateur::estConnecte()){
            parent::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            die();
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($get["login"]);
        parent::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "titre" => "Formulaire MiseAJour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
    }

    public static function afficherFormulaireMiseAJour(Array $get): void
    {
        if (!ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::getLoginUtilisateurConnecte() != $get["login"]){
            parent::afficherErreur("Vous devez être le propietere de cette compte pour la modifier.");
        }
        elseif ($get['mdp'] != $get['mdp2'])
            parent::afficherErreur("Mots de passe distincts.");
        else {
            $mdpHache = MotDePasse::hacher($get['mdp']);
            $get['mdpHache'] = $mdpHache;
            $utilisateur = (new UtilisateurRepository())->construireDepuisTableauSQL($get);
            if(!MotDePasse::verifier($get['Amdp'], $utilisateur->getMdpHache())){
                parent::afficherErreur("Ancien mot de passe incorrect.");
                die();
            }
            (new UtilisateurRepository())->miseAJours($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            parent::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/afficherFormulaireMiseAJours.php"]);
        }
    }

    

    public static function utilisateurCree(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        parent::afficherVue("vueGeneral.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);  //"redirige" vers la vue
    }

    public static function supprimer($get): void{
        if (!ConnexionUtilisateur::estConnecte() || !isset($get['login']) ||ConnexionUtilisateur::getLoginUtilisateurConnecte() != $get["login"]){
            parent::afficherErreur("Vous devez être connecte.");
            die();
        }
        $login = $get["login"];
        $userSupprime = (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        parent::afficherVue("vueGeneral.php", ["bienFait" => $userSupprime, "utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "login" => $login]);  //"redirige" vers la vue
    }

    public static function afficherFormulaireConnexion(): void
    {
        $session = Session::getInstance();
        $session->detruire();
        ConnexionUtilisateur::deconnecter();
        parent::afficherVueGeneralAvec("utilisateur/formulaireConnexion.php");
    }

    public static function connecter($get)
    {
        /** @var Utilisateur $utilisateur */
        $utilisateur  = (new UtilisateurRepository())->recupererParClePrimaire($get["login"]);
        if ($utilisateur != null){
            if (MotDePasse::verifier($_GET["mdp"], $utilisateur->getMdpHache())){
                /**@var Session $session */
                $session = Session::getInstance();
                if ($session->contient("_utilisateurConnecte"))
                    echo "<br> conexion OK";
                ConnexionUtilisateur::connecter($utilisateur->getLogin());
                parent::afficherVueGeneralAvec("utilisateur/utilisateurConnecte.php",  ["utilisateur" => $utilisateur,]);
            }
            else{
                parent::afficherErreur("Mot de passe incorrect.");
            }
        }
        else{
            parent::afficherErreur("Login ou Mot de passe incorrect.");
        }
    }
}