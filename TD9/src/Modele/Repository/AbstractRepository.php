<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire() : string;
    /** @return string[] */
    protected abstract function getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $tableau = array();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM {$this->getNomTable()}");
        foreach ($pdoStatement as $abstractDataObjectFormatTableau) {
            $tableau[] = $this->construireDepuisTableauSQL($abstractDataObjectFormatTableau);
        }
        return $tableau;
    }
    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from {$this->getNomTable()} WHERE {$this->getNomClePrimaire()}=:tagClePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(':tagClePrimaire' => $clePrimaire);
        $pdoStatement->execute($values);
        $tableau = $pdoStatement->fetch();
        if (isset($tableau[$this->getNomClePrimaire()]))
            return $this->construireDepuisTableauSQL($tableau);
        return null;
    }
    public function supprimer(string $valeurClePrimaire): bool
    {
        $sql = "DELETE FROM {$this->getNomTable()} WHERE {$this->getNomClePrimaire()}=:tagClePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(':tagClePrimaire' => $valeurClePrimaire);
        $pdoStatement->execute($values);
        if ($pdoStatement->rowCount() > 0) {
            return true;
        }
        return false;
    }
    public function ajouter(AbstractDataObject $object): void
    {
        $sql = "INSERT INTO {$this->getNomTable()} (" . implode(',', $this->getNomsColonnes()) . ") VALUES (:" . implode('Tag, :', $this->getNomsColonnes()) . "Tag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute($this->formatTableauSQL($object));
    }
    public function miseAJours(AbstractDataObject $object): void
    {
        $colonnesSansId = array_slice($this->getNomsColonnes(), 1);

        $setPart = implode(', ', array_map(function($colonne) {
            return "{$colonne}=:{$colonne}Tag";
        }, $colonnesSansId));

        $sql = "UPDATE {$this->getNomTable()} SET {$setPart} WHERE {$this->getNomClePrimaire()}=:{$this->getNomClePrimaire()}Tag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute($this->formatTableauSQL($object));
    }
}