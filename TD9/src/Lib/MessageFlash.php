<?php

namespace App\Covoiturage\Lib;

class MessageFlash
{
    // Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

    // $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void
    {
        switch ($type) {
            case "success":
                $_SESSION[self::$cleFlash]['success'][] = $message;
                break;
            case "warning":
                $_SESSION[self::$cleFlash]['warning'][] = $message;
                break;
            case "danger":
                $_SESSION[self::$cleFlash]['danger'][] = $message;
                break;
            case "info":
                $_SESSION[self::$cleFlash]['info'][] = $message;
                break;
        }
    }

    public static function contientMessage(string $type): bool
    {
        return isset($_SESSION[self::$cleFlash][$type]);
    }

    // Attention : la lecture doit détruire le message
    public static function lireMessages(string $typeAttendu): array
    {
        $messages = [];
        if (isset($_SESSION[self::$cleFlash]) && is_array($_SESSION[self::$cleFlash])) {
            foreach ($_SESSION[self::$cleFlash] as $type => $typeMessages) {
                if (is_array($typeMessages) && $typeAttendu == $type) {
                    foreach ($typeMessages as $key => $message) {
                        $messages[$type][] = $message;
                        unset($_SESSION[self::$cleFlash][$type][$key]);
                    }
                }
            }
        };
        return $messages;
    }

    public static function lireTousMessages(): array
    {
        $messages = [];

        if (isset($_SESSION[self::$cleFlash]) && is_array($_SESSION[self::$cleFlash])) {
            foreach ($_SESSION[self::$cleFlash] as $type => $typeMessages) {
                if (is_array($typeMessages)) {
                    foreach ($typeMessages as $key => $message) {
                        $messages[$type][] = $message;
                        unset($_SESSION[self::$cleFlash][$type][$key]);
                    }
                }
            }
        }
        return $messages;
    }
}