<?php
/** @var string $titre*/
/** @var string $cheminCorpsVue*/
/** @var bool $login */

use App\Covoiturage\Lib\ConnexionUtilisateur;

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/navstyles.css" />
    <title><?php echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="./controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="./controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="./controleurFrontal.php?action=afficherFormulairePreference">
                    <img src="https://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/TD7/heart.png">
                </a>
            </li>
            <?php if (ConnexionUtilisateur::estConnecte()): ?>
                <!-- pour sortir -->
                <li>
                    <a href="./controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur">
                        <img src="https://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/TD8/logout.png">
                    </a>
                </li>
                <!-- pour voir formulaire creation -->
                <li>
                    <a href="./controleurFrontal.php?action=afficherFormulaireCreaction&controleur=utilisateur">
                        <img src="https://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/TD8/add-user.png">
                    </a>
                </li>
                <!-- pour voir login perso -->
                <li>
                    <a href="./controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?=(ConnexionUtilisateur::getLoginUtilisateurConnecte());?>">
                        <img src="https://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/TD8/user.png">
                    </a>
                </li>
            <?php else: ?>
                <li>
                    <!-- pour s'econnecter-->
                    <a href="./controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur">
                        <img src="https://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/TD8/enter.png">
                    </a>
                </li>
            <?php endif; ?>
        </ul>
    </nav>
    <div>
        <?php
        /** @var string[][] $messagesFlash */
        foreach($messagesFlash as $type => $messagesFlashPourUnType) {
            // $type est l'une des valeurs suivantes : "success", "info", "warning", "danger"
            // $messagesFlashPourUnType est la liste des messages flash d'un type
            foreach ($messagesFlashPourUnType as $messageFlash) {
                echo <<< HTML
            <div class="alert alert-$type">
               $messageFlash
            </div>
            HTML;
            }
        }
        ?>
    </div>
</header>
<main>
    <?php require __DIR__ . "/{$cheminCorpsVue}"; ?>
</main>
<footer>
    <p>
        Site de covoiturage de...
    </p>
</footer>
</body>
</html>