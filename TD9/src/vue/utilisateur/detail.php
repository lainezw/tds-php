<?php

/** @var App\Covoiturage\Modele\Utilisateur $utilisateur */

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
    <h1>Details sur l'utilisateur M.<?=htmlspecialchars($utilisateur->getNom())?></h1>
    <p><?=htmlspecialchars($utilisateur)?></p>
</body>
</html>
