<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\Utilisateur;
#require_once __DIR__.'/../Modele/Utilisateur.php'; // chargement du modèle
class ControleurUtilisateur
{
// Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(string $login): void
    {
        $utilisateur = Utilisateur::recupererUtilisateurParLogin($login);
        if (empty($utilisateur))
            ControleurUtilisateur::afficherVue("erreur.php");
        else
            ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue
    }

    public static function afficherFormulaireCreation(array $user): void
    {
        $utilisateur = Utilisateur::construireDepuisTableauSQL($user);
        $utilisateur->ajouter();
        $utilisateurs = Utilisateur::recupererUtilisateurs();
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs, "titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/afficherFormulaireCreation.php"]);
    }

    public static function afficherFormulaire(): void
    {
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["titre" => "afficherDetail", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function utilisateurCree(): void
    {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue("vueGeneral.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);  //"redirige" vers la vue
    }
}

?>