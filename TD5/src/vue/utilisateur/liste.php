<?php
/** @var App\Covoiturage\Modele\Utilisateur[] $utilisateurs */
?>

<h1>Liste d'Utilisateurs</h1>
<?php foreach ($utilisateurs as $utilisateur):?>
    <p>Utilisateur
        <a href="./controleurFrontal.php?action=afficherDetail&login=<?=rawurlencode($utilisateur->getLogin())?>">
            <?=htmlspecialchars($utilisateur->getNom())?>
        </a>
    </p>
<?php endforeach;?>