<?php
/** @var App\Covoiturage\Modele\Utilisateur $utilisateur */
/** @var App\Covoiturage\Modele\Utilisateur[] $utilisateurs */

?>
<h1>Liste d'Utilisateurs avec un nouveau utilisateur</h1>
<h3>Le nouveau est </h3>
<p><?=htmlspecialchars($utilisateur)?></p>
<h3>Liste </h3>

<?php foreach ($utilisateurs as $utilisateur):?>
    <p>
        Utilisateur
        <a
            href="./controleurFrontal.php?action=afficherDetail&login=<?=rawurlencode($utilisateur->getLogin())?>">
            <?=htmlspecialchars($utilisateur->getNom())?>
        </a>
    </p>
<?php endforeach;?>
