<?php
use App\Covoiturage\Controleur\ControleurUtilisateur;
#require_once __DIR__.'/../src/Controleur/ControleurUtilisateur.php';
require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src/');

if (isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "accueil";
//echo $action;
switch ($action) {
    case 'afficherListe':
        ControleurUtilisateur::$action(); // Appel de la méthode statique $action de ControleurUtilisateur\\
        break;
    case 'afficherDetail':
        ControleurUtilisateur::afficherDetail($_GET['login']);
        break;
        case 'creerDepuisFormulaire':
        ControleurUtilisateur::afficherFormulaire();
        break;
    case 'afficherFormulaireCreation':
        ControleurUtilisateur::afficherFormulaireCreation($_GET);
        break;
    case 'utilisateurCree':
        ControleurUtilisateur::utilisateurCree();
        break;
    default:
        echo "C quoi ton probleme??";
        break;
}

?>