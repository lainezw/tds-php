<?php 

$prenom = "Renault"; 
$nom = "Leblanc"; 
$login = "leblancj"; 


$utilisateur = array(
    "prenom" => "Renault",
    "nom" => "Leblanc",
    "login" => "leblancj",
);

$utilisateurs = array(
    array(
        "prenom" => "1",
        "nom" => "1",
        "login" => "1",
        
    ),
    array(
        "prenom" => "2",
        "nom" => "2",
        "login" => "2",
        
    ),
    
);

$utilisateursVide = array();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        <h2>3 variables</h2>
        <p><?="Utilisateur $prenom $nom de login $login"?></p>
        <h2>un array indexe par noms</h2>
        <p><?="Utilisateur {$utilisateur['prenom']} {$utilisateur['nom']} de login {$utilisateur['login']}"?></p>
        <h2>un array indexe par nombre avec array</h2>
        <ul>
            <?php foreach ($utilisateurs as $utilisateur):?>
                <p><?="Utilisateur {$utilisateur['prenom']} {$utilisateur['nom']} de login {$utilisateur['login']}"?></p>
            <?php endforeach;?>
        </ul>
        <h2>Un array vide?</h2>
        <?php if(empty($utilisateursVide)): ?>
            <p>C'est vide</p>
        <?php else: ?>
            <p>Ce n'est pas vide</p>
        <?php endif; ?>
    </body>
</html> 