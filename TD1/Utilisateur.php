<?php

class Utilisateur {

    private string $prenom;
    private string $nom;

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login): void
    {
        $this->login = $this->verificationLongeur($login);
    }
    private string $login;

    public function getLogin():string
    {
        return $this->login;
    }
    public function getPrenom():string
    {
        return $this->prenom;
    }

    // un getter
    public function getNom():string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom,
    ) {
        $this->login = $this->verificationLongeur($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString():string {
        return "Utilisateur $this->prenom $this->nom de login $this->login";
    }

    public function verificationLongeur(string $login):string
    {
        if(strlen($login)>64)
            return substr($login,0,(64-strlen($login)));
        else
            return $login;
    }
}